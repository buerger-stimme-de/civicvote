#
# Build the final image containing the application only
#

# must match the version we compile against!
FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app
EXPOSE 80

ENTRYPOINT ["dotnet", "Vote.dll"]

COPY target /app
