# Civic Vote

## About

"CivicVote" is the internal name for the software running [https://b�rger-stimme.de](https://b�rger-stimme.de). It is a platform allowing users to give feedback about interactions with German authorities. CivicVote collects all user reports, groups them, and provides analyses. This way the platform can help pinpointing problems that many citizens face when dealing with authorities. Mayors or chiefs of authorities can use these insights to improve their institutions, e.g. by making them more customer-friendly.

## License
The project is made available under the [AGPL license](https://gitlab.com/buerger-stimme-de/civicvote/-/blob/master/LICENSE.md).

## Technology Stack
- GitLab to host the repository, pipelines, artifacts
- [ASP.NET Core 6.0](https://docs.microsoft.com/en-us/aspnet/core/) with [C#](https://docs.microsoft.com/de-de/dotnet/csharp/whats-new/csharp-9) and [TypeScript](https://www.typescriptlang.org/) as programming languages
- [MySQL](https://www.mysql.com/) database accessed via [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
- [Piranha CMS](https://piranhacms.org/) used for fixed pages
- [Razor pages](https://docs.microsoft.com/en-us/aspnet/core/razor-pages/) for dynamic pages (and a few [MVC Controllers](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/actions) as well)
- [Renovate](https://github.com/renovatebot/renovate) to update all dependencies automatically
- [Docker](https://www.docker.com/) for full automated deployment

## Status
The project is in a very early alpha stage.

## Installation
In short:

    cd developers
    docker-compose up -d

    cd src\Vote
    dotnet run

Then browse to [http://localhost:5000/](http://localhost:5000/) and login as ``first@user.com`` with password ``piranha``.

For more details about how to compile and start the project, please refer to our [Getting Started wiki pages](https://gitlab.com/buerger-stimme-de/civicvote/-/wikis/Developer/Getting-started).

## Tests
Tests run in the pipeline as well. For every C# project create a test project which hosts the unit/component tests as well as integration and end to end tests.

## Configuration
CivicVote loads its default configuration from ``src\Vote\config\appsettings.default.json``. This file is copied there during development from [``src\config\appsettings.default.json``](https://gitlab.com/buerger-stimme-de/civicvote/-/blob/master/src/config/appsettings.default.json). This file contains default values which work with ``docker-compose`` as described above.

If needed these default values can be overwritten by creating ``src\Vote\config\appsettings.json``.

Highest precedence have configuration settings define using environment variables. These need to be prefixed by ``CV_``. Section name and setting name is separated by ``:``. Examples for setting under Windows: ``set CV_DatabaseConnectionVote:Server=localhost`` and ``set CV_DatabaseConnectionVote:Port=3306``

## Getting Help
Contact us via our [Community room on Gitter](https://gitter.im/buerger-stimme-de/community/).

## Getting Involved
Your help is appreciated. Checkout the [CONTRIBUTION.md](https://gitlab.com/buerger-stimme-de/civicvote/-/blob/master/CONTRIBUTING.md) for further information.
