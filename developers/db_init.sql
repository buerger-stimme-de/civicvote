create user civicvote@'%' identified by 'civicvote';
create database civicvote;
grant all privileges on civicvote.* to civicvote@'%';

create user piranha@'%' identified by 'piranha';
create database piranha;
grant all privileges on piranha.* to piranha@'%';

flush privileges;
