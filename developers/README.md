Contains all files which are relevant for developing on the local machine.

# Setup Local machine
Run `docker-compose up -d` in this directory to start all dependencies needed to run
Civicvote.

- CMS database is `piranha` (user `piranha`, password `piranha`)
- Civicvote database is `civicvote` (user `civicvote`, password `civicvote`)
- MySql can be found at `localhost:3333`and the root password is `root`
- initial password in Piranha: user `first@user.com`, password `piranha`
