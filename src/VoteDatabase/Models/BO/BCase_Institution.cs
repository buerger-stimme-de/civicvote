﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// Helper table to customize many-to-many relation between BCase and Institution
    /// https://www.entityframeworktutorial.net/efcore/configure-many-to-many-relationship-in-ef-core.aspx
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "Using underscore because this is a JOIN table, just a helper")]
    public class BCase_Institution
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public BCase_Institution()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        public BCase_Institution(BCase bCase, Institution institution)
        {
            BCase = bCase;
            Institution = institution;
            Valuations = new List<Valuation>();
        }

        [Key]
        public uint Id { get; set; }

        [ForeignKey(nameof(BCase))]
        public uint FkBCase { get; set; }
        public BCase BCase { get; set; }

        [ForeignKey(nameof(Institution))]
        public uint FkInstitution { get; set; }
        public Institution Institution { get; set; }

        /// <summary>
        /// Valuations for <see cref="Institution"/> for this <see cref="BCase"/>
        /// </summary>
        public ICollection<Valuation> Valuations { get; private set; }
    }
}
