﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VoteDatabase.Models.Enums;

namespace VoteDatabase.Models.BO
{
    /// <summary>
    /// Valuation of institution with respect to a particular BCase
    /// </summary>
    public class Valuation
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable. - Only for Entity Framework
        [Obsolete("Only intended for de-serialization. Caller must make sure that non-nullable properties are properly initialized.")]
        public Valuation()
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        {
        }

        public Valuation(string rational, string fkUser, uint fkCaseInstitution, ValuationRating rating)
        {
            Rational = rational;
            FkUser = fkUser;
            FkCaseInstitution = fkCaseInstitution;
            Rating = rating;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public uint Id { get; set; }

        [ForeignKey(nameof(CaseInstitution))]
        public uint FkCaseInstitution { get; set; }
        public virtual BCase_Institution? CaseInstitution { get; set; }

        [ForeignKey(nameof(User))]
        public string FkUser { get; set; }
        public virtual User? User { get; set; }

        /// <summary>
        /// Valuation rating
        /// </summary>
        public ValuationRating Rating { get; set; }

        /// <summary>
        /// Statement of reasons for rating
        /// </summary>
        [Column(TypeName = "VARCHAR")]
        [StringLength(1024)]
        public string Rational { get; set; }

        /// <summary>
        /// (Last change) Date of valuation
        /// </summary>
        public DateTime Date { get; set; }
    }
}
