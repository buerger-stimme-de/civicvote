﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VoteDatabase.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Vote");

            migrationBuilder.EnsureSchema(
                name: "Identity");

            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "HtmlBlock",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Html = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HtmlBlock", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "InstitutionBranch",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstitutionBranch", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Role",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Name = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    NormalizedName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "User",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CustomTag2 = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserName = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    NormalizedUserName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    NormalizedEmail = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EmailConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    PasswordHash = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SecurityStamp = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ConcurrencyStamp = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PhoneNumber = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PhoneNumberConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetime(6)", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "InstitutionType",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FkBranch = table.Column<uint>(type: "int unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstitutionType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InstitutionType_InstitutionBranch_FkBranch",
                        column: x => x.FkBranch,
                        principalSchema: "Vote",
                        principalTable: "InstitutionBranch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "RoleClaim",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", maxLength: 100, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FkRole = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClaimType = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClaimValue = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaim_Role_FkRole",
                        column: x => x.FkRole,
                        principalSchema: "Identity",
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "BCase",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Summary = table.Column<string>(type: "VARCHAR(1024)", maxLength: 1024, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FkCreator = table.Column<string>(type: "varchar(100)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Category = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BCase", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BCase_User_FkCreator",
                        column: x => x.FkCreator,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "User_Role",
                schema: "Identity",
                columns: table => new
                {
                    FkUser = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FkRole = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Role", x => new { x.FkUser, x.FkRole });
                    table.ForeignKey(
                        name: "FK_User_Role_Role_FkRole",
                        column: x => x.FkRole,
                        principalSchema: "Identity",
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_Role_User_FkUser",
                        column: x => x.FkUser,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "UserClaim",
                schema: "Identity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", maxLength: 100, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FkUser = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClaimType = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClaimValue = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaim_User_FkUser",
                        column: x => x.FkUser,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "UserLogin",
                schema: "Identity",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ProviderKey = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ProviderDisplayName = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FkUser = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogin", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogin_User_FkUser",
                        column: x => x.FkUser,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "UserToken",
                schema: "Identity",
                columns: table => new
                {
                    FkUser = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LoginProvider = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Value = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => new { x.FkUser, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserToken_User_FkUser",
                        column: x => x.FkUser,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Institution",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FkInstitutionType = table.Column<uint>(type: "int unsigned", nullable: false),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PLZ = table.Column<ushort>(type: "smallint unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Institution", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Institution_InstitutionType_FkInstitutionType",
                        column: x => x.FkInstitutionType,
                        principalSchema: "Vote",
                        principalTable: "InstitutionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "BCase_Institution",
                schema: "Vote",
                columns: table => new
                {
                    FkBCase = table.Column<uint>(type: "int unsigned", nullable: false),
                    FkInstitution = table.Column<uint>(type: "int unsigned", nullable: false),
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BCase_Institution", x => new { x.FkBCase, x.FkInstitution });
                    table.UniqueConstraint("AK_BCase_Institution_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BCase_Institution_BCase_FkBCase",
                        column: x => x.FkBCase,
                        principalSchema: "Vote",
                        principalTable: "BCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BCase_Institution_Institution_FkInstitution",
                        column: x => x.FkInstitution,
                        principalSchema: "Vote",
                        principalTable: "Institution",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "BEvent",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Summary = table.Column<string>(type: "VARCHAR(1024)", maxLength: 1024, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Text = table.Column<string>(type: "VARCHAR(10240)", maxLength: 10240, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    AttachmentFile = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    FkInstitution = table.Column<uint>(type: "int unsigned", nullable: false),
                    FkBCase = table.Column<uint>(type: "int unsigned", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BEvent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BEvent_BCase_FkBCase",
                        column: x => x.FkBCase,
                        principalSchema: "Vote",
                        principalTable: "BCase",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BEvent_Institution_FkInstitution",
                        column: x => x.FkInstitution,
                        principalSchema: "Vote",
                        principalTable: "Institution",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Valuation",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FkCaseInstitution = table.Column<uint>(type: "int unsigned", nullable: false),
                    FkUser = table.Column<string>(type: "varchar(100)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Rating = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Rational = table.Column<string>(type: "VARCHAR(1024)", maxLength: 1024, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Valuation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Valuation_BCase_Institution_FkCaseInstitution",
                        column: x => x.FkCaseInstitution,
                        principalSchema: "Vote",
                        principalTable: "BCase_Institution",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Valuation_User_FkUser",
                        column: x => x.FkUser,
                        principalSchema: "Identity",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                schema: "Identity",
                table: "User",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CustomTag2", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "4e08e274-071c-4477-89a3-b12cae60ef9d", 0, "09827715-73ff-4852-983d-d5e6267c51ce", "tag", "first@user.com", true, true, new DateTimeOffset(new DateTime(2021, 10, 28, 2, 17, 47, 21, DateTimeKind.Unspecified).AddTicks(8944), new TimeSpan(0, 0, 0, 0, 0)), "FIRST@USER.COM", "FIRST@USER.COM", "a", "000-123456789", false, "f353695f-4f0f-47d8-abcd-dc8206671d5b", false, "first@user.com" });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "InstitutionBranch",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1u, "Behörde" });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "BCase",
                columns: new[] { "Id", "Category", "CreationDate", "FkCreator", "Summary" },
                values: new object[] { 1u, 101, new DateTime(2021, 10, 28, 2, 17, 47, 24, DateTimeKind.Utc).AddTicks(9503), "4e08e274-071c-4477-89a3-b12cae60ef9d", "Well done" });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "InstitutionType",
                columns: new[] { "Id", "FkBranch", "Name" },
                values: new object[] { 1u, 1u, "Super-Amt" });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "Institution",
                columns: new[] { "Id", "FkInstitutionType", "Name", "PLZ" },
                values: new object[] { 1u, 1u, "Super-Amt Winsen", (ushort)12345 });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "BCase_Institution",
                columns: new[] { "FkBCase", "FkInstitution", "Id" },
                values: new object[] { 1u, 1u, 1u });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "BEvent",
                columns: new[] { "Id", "AttachmentFile", "CreationDate", "FkBCase", "FkInstitution", "Summary", "Text", "Type" },
                values: new object[] { 1u, "file.txt", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1u, 1u, "Event summary", "Event text", 0 });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "BEvent",
                columns: new[] { "Id", "AttachmentFile", "CreationDate", "FkBCase", "FkInstitution", "Summary", "Text", "Type" },
                values: new object[] { 2u, "file2.txt", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1u, 1u, "Event summary2", "Second Event text", 0 });

            migrationBuilder.InsertData(
                schema: "Vote",
                table: "Valuation",
                columns: new[] { "Id", "Date", "FkCaseInstitution", "FkUser", "Rating", "Rational" },
                values: new object[] { 1u, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1u, "4e08e274-071c-4477-89a3-b12cae60ef9d", (byte)1, "Optimal" });

            migrationBuilder.CreateIndex(
                name: "IX_BCase_FkCreator",
                schema: "Vote",
                table: "BCase",
                column: "FkCreator");

            migrationBuilder.CreateIndex(
                name: "IX_BCase_Institution_FkInstitution",
                schema: "Vote",
                table: "BCase_Institution",
                column: "FkInstitution");

            migrationBuilder.CreateIndex(
                name: "IX_BEvent_FkBCase",
                schema: "Vote",
                table: "BEvent",
                column: "FkBCase");

            migrationBuilder.CreateIndex(
                name: "IX_BEvent_FkInstitution",
                schema: "Vote",
                table: "BEvent",
                column: "FkInstitution");

            migrationBuilder.CreateIndex(
                name: "IX_Institution_FkInstitutionType",
                schema: "Vote",
                table: "Institution",
                column: "FkInstitutionType");

            migrationBuilder.CreateIndex(
                name: "IX_InstitutionType_FkBranch",
                schema: "Vote",
                table: "InstitutionType",
                column: "FkBranch");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "Identity",
                table: "Role",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaim_FkRole",
                schema: "Identity",
                table: "RoleClaim",
                column: "FkRole");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "Identity",
                table: "User",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "Identity",
                table: "User",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_Role_FkRole",
                schema: "Identity",
                table: "User_Role",
                column: "FkRole");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_FkUser",
                schema: "Identity",
                table: "UserClaim",
                column: "FkUser");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogin_FkUser",
                schema: "Identity",
                table: "UserLogin",
                column: "FkUser");

            migrationBuilder.CreateIndex(
                name: "IX_Valuation_FkCaseInstitution",
                schema: "Vote",
                table: "Valuation",
                column: "FkCaseInstitution");

            migrationBuilder.CreateIndex(
                name: "IX_Valuation_FkUser",
                schema: "Vote",
                table: "Valuation",
                column: "FkUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BEvent",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "HtmlBlock",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "RoleClaim",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "User_Role",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "UserClaim",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "UserLogin",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "UserToken",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "Valuation",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "Role",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "BCase_Institution",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "BCase",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "Institution",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "User",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "InstitutionType",
                schema: "Vote");

            migrationBuilder.DropTable(
                name: "InstitutionBranch",
                schema: "Vote");
        }
    }
}
