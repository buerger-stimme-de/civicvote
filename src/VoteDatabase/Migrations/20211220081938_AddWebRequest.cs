﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VoteDatabase.Migrations
{
    public partial class AddWebRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Identity",
                table: "User",
                keyColumn: "Id",
                keyValue: "4e08e274-071c-4477-89a3-b12cae60ef9d");

            migrationBuilder.CreateTable(
                name: "WebRequest",
                schema: "Vote",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "int unsigned", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Timestamp = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Identity = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RemoteIpAddress = table.Column<string>(type: "varchar(45)", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Method = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Path = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Query = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UserAgent = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Referer = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    IsWebSocket = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebRequest", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                schema: "Identity",
                table: "User",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CustomTag2", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "7b4fb693-d218-4849-af38-6605991f7e90", 0, "c82c652a-e530-47f6-a616-01c5ad984e33", "tag", "first@user.com", true, true, new DateTimeOffset(new DateTime(2021, 12, 20, 8, 19, 37, 303, DateTimeKind.Unspecified).AddTicks(3045), new TimeSpan(0, 0, 0, 0, 0)), "FIRST@USER.COM", "FIRST@USER.COM", "a", "000-123456789", false, "7aca1116-e259-4ae5-8c81-3037f2fea1f0", false, "first@user.com" });

            migrationBuilder.UpdateData(
                schema: "Vote",
                table: "BCase",
                keyColumn: "Id",
                keyValue: 1u,
                columns: new[] { "CreationDate", "FkCreator" },
                values: new object[] { new DateTime(2021, 12, 20, 8, 19, 37, 304, DateTimeKind.Utc).AddTicks(3769), "7b4fb693-d218-4849-af38-6605991f7e90" });

            migrationBuilder.UpdateData(
                schema: "Vote",
                table: "Valuation",
                keyColumn: "Id",
                keyValue: 1u,
                column: "FkUser",
                value: "7b4fb693-d218-4849-af38-6605991f7e90");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WebRequest",
                schema: "Vote");

            migrationBuilder.DeleteData(
                schema: "Identity",
                table: "User",
                keyColumn: "Id",
                keyValue: "7b4fb693-d218-4849-af38-6605991f7e90");

            migrationBuilder.InsertData(
                schema: "Identity",
                table: "User",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CustomTag2", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "4e08e274-071c-4477-89a3-b12cae60ef9d", 0, "09827715-73ff-4852-983d-d5e6267c51ce", "tag", "first@user.com", true, true, new DateTimeOffset(new DateTime(2021, 10, 28, 2, 17, 47, 21, DateTimeKind.Unspecified).AddTicks(8944), new TimeSpan(0, 0, 0, 0, 0)), "FIRST@USER.COM", "FIRST@USER.COM", "a", "000-123456789", false, "f353695f-4f0f-47d8-abcd-dc8206671d5b", false, "first@user.com" });

            migrationBuilder.UpdateData(
                schema: "Vote",
                table: "BCase",
                keyColumn: "Id",
                keyValue: 1u,
                columns: new[] { "CreationDate", "FkCreator" },
                values: new object[] { new DateTime(2021, 10, 28, 2, 17, 47, 24, DateTimeKind.Utc).AddTicks(9503), "4e08e274-071c-4477-89a3-b12cae60ef9d" });

            migrationBuilder.UpdateData(
                schema: "Vote",
                table: "Valuation",
                keyColumn: "Id",
                keyValue: 1u,
                column: "FkUser",
                value: "4e08e274-071c-4477-89a3-b12cae60ef9d");
        }
    }
}
