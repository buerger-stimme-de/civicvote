﻿using System;
using System.Collections.Generic;

[assembly: CLSCompliant(false)]
namespace SharedHelpers.Misc
{
    public static class Debug
    {
        /// <summary>
        /// Break execution in debug mode.
        /// </summary>
        public static void Break()
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debugger.Break();
            }
#endif
        }

#if DEBUG
        private static readonly HashSet<string> BreakOnceKeys = new();
#endif

        /// <summary>
        /// Break execution in debug mode if condition is met.
        /// </summary>
        /// <param name="breakOnTrue">Function to check for break</param>
        /// <param name="breakOnceKey">If set, break will only be called once for given key</param>
        public static void Break(Func<bool> breakOnTrue, string? breakOnceKey = null)
        {
#if DEBUG
            // Break
            // - Only when running in debugger
            // - When check function returns true or is not provided
            // - When key is provided for the first time (i.e. HashSet.Add succeeds) or not at all
            if (System.Diagnostics.Debugger.IsAttached
                && (breakOnTrue == null || breakOnTrue())
                && (breakOnceKey == null || BreakOnceKeys.Add(breakOnceKey)))
            {
                System.Diagnostics.Debugger.Break();
            }
#endif
        }
    }
}
