﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedHelpers.Misc
{
    public class HelpersException : Exception
    {
        public HelpersException()
        : base()
        {
        }

        public HelpersException(string message)
            : base(message)
        {
        }

        public HelpersException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
