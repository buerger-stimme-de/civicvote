﻿using System.Globalization;

namespace SharedHelpers.Misc
{
    public static class StringExtensions
    {
        public static int ToInt(this string input)
        {
            return int.Parse(input, CultureInfo.InvariantCulture);
        }
    }
}
