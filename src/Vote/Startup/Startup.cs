using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Piranha;
using Vote.Constants;
using Vote.Controllers;
using Vote.Models.Settings;
using Vote.Support;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;
using Roles = VoteDatabase.Constants.Roles;

// Ignore spelling: httpdocs

namespace Vote.Startup
{
#pragma warning disable CA1724 // Type names should not match namespaces
    public class Startup
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // https://www.puresourcecode.com/dotnet/net-core/using-an-in-memory-repository-keys-will-not-be-persisted-to-storage-asp-net-core-under-iis/
            services.AddDataProtection().PersistKeysToFileSystem(new DirectoryInfo(Path.Combine(Program.DataAndLogsFolderProvider.Root, "Keys")));

            var voteSettings = ConfigureDatabaseAndSettings(services);

            services.AddControllersWithViews();

            // Razor pages folder is explicitly used by Piranha. Other pages should go to dedicate Razor Area.
            services.AddRazorPages(options =>
            {
                // Require login for /Vote/*
                options.Conventions.AuthorizeAreaFolder("Vote", "/");

                // Require "Manage" permission for /Manage/*
                options.Conventions.AuthorizeAreaFolder("Manage", "/", PermissionPolicy.Manage);
            }).WithRazorPagesRoot("/PagesCms");
            AddPiranha(services, voteSettings.DatabaseConnectionCms, voteSettings.UploadsCmsPath);

            ConfigureAuthorizationAndAuthentication(services);

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = _ => true;
            });
        }

        private static void ConfigureAuthorizationAndAuthentication(IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PermissionPolicy.Manage, policy => policy.RequireRole(Roles.Administrator));
                options.AddPolicy(PermissionPolicy.Cms, policy => policy.RequireRole(Roles.PiranhaAdministrator));

                // Override Piranha policies after AddPiranha()
                foreach (var policy in Piranha.Manager.Permission.All())
                {
                    // Grant all Piranha policies to Roles.PiranhaAdministrator
                    options.AddPolicy(policy, p => p.RequireRole(Roles.PiranhaAdministrator));
                }

                foreach (var permission in Piranha.Security.Permission.All())
                {
                    // Grant all Piranha permissions to Roles.PiranhaAdministrator
                    options.AddPolicy(permission, policy => policy.RequireRole(Roles.PiranhaAdministrator));
                }
            });

            // https://docs.microsoft.com/en-us/aspnet/core/security/authentication/social/additional-claims?view=aspnetcore-6.0
            services.AddAuthentication();

            // TODO: Add password-less authentication via email
            // https://andrewlock.net/implementing-custom-token-providers-for-passwordless-authentication-in-asp-net-core-identity/
        }

        private VoteSettings ConfigureDatabaseAndSettings(IServiceCollection services)
        {
            var voteSettings = new VoteSettings();
            Configuration.Bind(voteSettings);
            SharedHelpers.Misc.RecursiveValidator.Validate(voteSettings);
            services.Configure<VoteSettings>(Configuration);

            var connectionStringVote = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder
            {
                Server = voteSettings.DatabaseConnectionVote.Server,
                Port = voteSettings.DatabaseConnectionVote.Port,
                Database = voteSettings.DatabaseConnectionVote.Database,
                Password = voteSettings.DatabaseConnectionVote.Password,
                UserID = voteSettings.DatabaseConnectionVote.Username,
            }.ConnectionString;

            services.AddDbContextPool<VoteDbContext>(builder =>
            {
                try
                {
                    builder.UseMySql(
                        connectionStringVote,
                        ServerVersion.AutoDetect(connectionStringVote),
                        options =>
                        {
                            options.SchemaBehavior(
                                Pomelo.EntityFrameworkCore.MySql.Infrastructure.MySqlSchemaBehavior.Translate,
                                (schema, entity) => $"{schema}{(schema == null ? null : ".")}{entity}");
                            options.EnableRetryOnFailure();
                        });
                }
                catch (Exception ex)
                {
                    voteSettings.DatabaseConnectionVote.Password = "__REMOVED__";
                    var databaseConnectionVoteValues = System.Text.Json.JsonSerializer.Serialize(voteSettings.DatabaseConnectionVote);
                    throw new VoteException(
                        $"Failed to access database. Please fix configuration '{nameof(VoteSettings.DatabaseConnectionVote)}'." +
                        $" Current values: {databaseConnectionVoteValues}",
                        ex);
                }
            });

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<User>(options =>
            {
                options.SignIn.RequireConfirmedAccount = true;
                options.Password.RequireNonAlphanumeric = true;
            }).AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<VoteDbContext>();

            return voteSettings;
        }

        private static void AddPiranha(
            IServiceCollection services,
            MySqlSettings databaseConnectionCms,
            string voteSettingsUploadsCmsPath)
        {
            var connectionStringCms = new MySql.Data.MySqlClient.MySqlConnectionStringBuilder
            {
                Server = databaseConnectionCms.Server,
                Port = databaseConnectionCms.Port,
                Database = databaseConnectionCms.Database,
                Password = databaseConnectionCms.Password,
                UserID = databaseConnectionCms.Username,
            }.ConnectionString;

            // Service setup
            services.AddPiranha(options =>
            {
#if DEBUG
                // This will enable automatic reload of .cshtml without restarting the application. However since
                // this adds a slight overhead it should not be enabled in production.
                options.AddRazorRuntimeCompilation = true;
#endif

                options.UseCms(o =>
                {
                    o.UseStartpageRouting = true;
                    o.UsePageRouting = true;
                    //o.UseSiteRouting = false;
                    //o.UseSitemapRouting = false;
                });
                options.UseManager(o =>
                {
                    //o.XsrfCookieName = "MY_ANTIFORGERY_COOKIE";
                    //o.XsrfHeaderName = "test"; // null not supported by piranha.util.js
                });

                options.UseFileStorage(naming: Piranha.Local.FileStorageNaming.UniqueFolderNames);
                options.UseImageSharp();
                options.UseTinyMCE();
                options.UseMemoryCache();

                options.UseEF<Piranha.Data.EF.MySql.MySqlDb>(db =>
                {
                    try
                    {
                        db.UseMySql(
                            connectionStringCms,
                            ServerVersion.AutoDetect(connectionStringCms),
                            options =>
                            {
                                options.EnableRetryOnFailure();
                            });
                    }
                    catch (Exception ex)
                    {
                        databaseConnectionCms.Password = "__REMOVED__";
                        var databaseConnectionCmsValues = System.Text.Json.JsonSerializer.Serialize(databaseConnectionCms);
                        throw new VoteException(
                            $"Failed to access database. Please fix configuration '{nameof(VoteSettings.DatabaseConnectionCms)}'." +
                            $" Current values: {databaseConnectionCmsValues}",
                            ex);
                    }
                });
            });

            services.AddPiranhaFileStorage(basePath: voteSettingsUploadsCmsPath, baseUrl: "~/cms/", naming: Piranha.Local.FileStorageNaming.UniqueFolderNames);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
#pragma warning disable CA1506 // Avoid excessive class coupling
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
#pragma warning restore CA1506 // Avoid excessive class coupling
            IApi api,
            Config config,
            Microsoft.Extensions.Options.IOptions<VoteSettings> voteSettings)
        {
            if (config == null) { throw new ArgumentNullException(nameof(config)); }
            if (voteSettings == null) { throw new ArgumentNullException(nameof(voteSettings)); }

            // Cache-Control must be define early!
            app.UseStaticFiles(new StaticFileOptions()
            {
                HttpsCompression = Microsoft.AspNetCore.Http.Features.HttpsCompressionMode.Compress,
                OnPrepareResponse = (context) =>
                {
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromDays(30),
                    };
                },
            });

            // Create static file provider for Piranha's uploads folder
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.GetFullPath(voteSettings.Value.UploadsCmsPath)), // must match basePath passed to AddPiranhaFileStorage()
                RequestPath = "/cms",
                OnPrepareResponse = (context) =>
                {
                    var headers = context.Context.Response.GetTypedHeaders();
                    headers.CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromDays(30),
                    };
                },
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                //app.UseStatusCodePages();
            }
            else
            {
                app.UseExceptionHandler(Areas.UrlPaths.Error);
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCookiePolicy();

            app.UseMiddleware<WebRequestWriter>();

            // First configure Piranha, then set up authorization (otherwise CMS not accessible)
            ConfigurePiranha(app, api, config);

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=" + HomeController.Name + "}/{action=" + HomeController.ActionIndex + "}/{id?}");
                endpoints.MapRazorPages();
            });
        }

        private static void ConfigurePiranha(IApplicationBuilder app, IApi api, Config config)
        {
            // Initialize Piranha
            App.Init(api);

            // Increase (AJAX) request timeout for Piranha to 60 seconds.
            config.ManagerXhrTimeout = 60;

            // Configure cache level
            App.CacheLevel = Piranha.Cache.CacheLevel.Full;

            // Register custom components
            App.Blocks.Register<Models.Cms.Blocks.FormBlock>();
            App.Blocks.Register<Models.Cms.Blocks.ButtonBlock>();
            App.Blocks.Register<Models.Cms.Blocks.UploadBlock>();
            App.Blocks.Register<Models.Cms.Blocks.Header1Block>();
            App.Blocks.Register<Models.Cms.Blocks.Header2Block>();

            // Build content types
            new Piranha.AttributeBuilder.ContentTypeBuilder(api)
                .AddAssembly(typeof(Startup).Assembly)
                .Build()
                .DeleteOrphans();

            // Configure Tiny MCE
            Piranha.Manager.Editor.EditorConfig.FromFile(VoteConfig.TinyMceEditorConfigFilePath);

            // Middle-ware setup
            app.UsePiranha(options =>
            {
                options.UseManager();
                options.UseTinyMCE();
                //options.UseIdentity();
            });
            app.UsePiranhaManager();
        }
    }
}
