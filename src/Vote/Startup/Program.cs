using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Serilog;
using Vote.Constants;
using Vote.Support;
using VoteDatabase.DataAccess;

namespace Vote.Startup
{
    public static class Program
    {
        internal static Exception? StartupException { get; set; }
        internal static PhysicalFileProvider DataAndLogsFolderProvider { get; } = new PhysicalFileProvider(Path.GetFullPath(VoteConfig.ConfigDirectory));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Main() needs to catch all exceptions")]
        public static async Task Main(string[] args)
        {
            try
            {
                await CheckConfigurationFiles().ConfigureAwait(false);

                var host = CreateHostBuilder(args).Build();

                await MigrateDatabase(host).ConfigureAwait(false);

                host.Run();
            }
            catch (Exception ex)
            {
                StartupException = ex;
                ExceptionCreateHostBuilder().Build().Run();
            }
        }

        private static async Task CheckConfigurationFiles()
        {
#if DEBUG
            // For development we copy all files from ../config to ./config
            // For deployment all configuration files are expected to be already present in ./config.

            if (!Directory.Exists(VoteConfig.ConfigDirectory))
            {
                Directory.CreateDirectory(VoteConfig.ConfigDirectory);
            }

            var parentConfigDirectory = Path.Combine("..", VoteConfig.ConfigDirectory);
            if (Directory.Exists(parentConfigDirectory))
            {
                foreach (var file in Directory.GetFiles(parentConfigDirectory))
                {
                    var destFile = Path.Combine(VoteConfig.ConfigDirectory, Path.GetFileName(file));
                    File.Copy(file, destFile, true);

                    // In appsettings files, find beginning of JSON content and insert DO NOT OVERWRITE warning.
                    if (destFile.Contains("appsettings", StringComparison.InvariantCultureIgnoreCase))
                    {
                        const string exclamationLine1 = "\"WARNING_IN\":\"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\",";
                        const string warningText = "\"WARNING\":\"DO NOT CHANGE THIS FILE AS IT IS OVERWRITTEN. CHANGE FILE IN ../../config INSTEAD!\",";
                        const string exclamationLine2 = "\"WARNING_OUT\":\"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\",";

                        var content = await File.ReadAllTextAsync(destFile).ConfigureAwait(false);
                        if (content.Contains(warningText, StringComparison.InvariantCulture))
                        {
                            throw new VoteException($"Source settings {file} contains DO NOT OVERWRITE warning. This should never happen.");
                        }

                        var firstContentPosition = content.IndexOf("{", StringComparison.InvariantCulture) + 1;
                        content = content[0..firstContentPosition] + Environment.NewLine + Environment.NewLine +
                            exclamationLine1 + Environment.NewLine +
                            warningText + Environment.NewLine +
                            exclamationLine2 + Environment.NewLine + Environment.NewLine +
                            content[firstContentPosition..];
                        await File.WriteAllTextAsync(destFile, content).ConfigureAwait(false);
                    }
                }
            }
#endif

            if (DateTime.Now.Ticks == 0)
            {
                // Workaround for disabling error in Release mode:
                //Error CS1998  This async method lacks 'await' operators and will run synchronously.
                await Task.Delay(0).ConfigureAwait(false);
            }

            if (!File.Exists(VoteConfig.AppSettingsDefaultFilePath))
            {
                throw new VoteException($"Required default settings file {Path.GetFullPath(VoteConfig.AppSettingsDefaultFilePath)} does not exist.");
            }

            if (!File.Exists(VoteConfig.TinyMceEditorConfigFilePath))
            {
                throw new VoteException($"Required editor configuration file {VoteConfig.TinyMceEditorConfigFilePath} does not exist.");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile(DataAndLogsFolderProvider, VoteConfig.AppSettingsDefaultFile, optional: false, reloadOnChange: false)
               .AddJsonFile(DataAndLogsFolderProvider, VoteConfig.AppSettingsFile, optional: true, reloadOnChange: false)
               .AddEnvironmentVariables()
               .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    // Remove default JSON sources set by ConfigureWebHostDefaults
                    var jsonSources = config.Sources.Where(s => s is Microsoft.Extensions.Configuration.Json.JsonConfigurationSource).ToList();
                    foreach (var jsonSource in jsonSources)
                    {
                        config.Sources.Remove(jsonSource);
                    }

                    config.AddJsonFile(DataAndLogsFolderProvider, VoteConfig.AppSettingsDefaultFile, optional: false, reloadOnChange: false);
                    config.AddJsonFile(DataAndLogsFolderProvider, VoteConfig.AppSettingsFile, optional: true, reloadOnChange: false);
                    config.AddEnvironmentVariables("CV_");
                })
                .UseSerilog();
        }

        public static IHostBuilder ExceptionCreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<ExceptionStartup>();
                });
        }

        private static async Task MigrateDatabase(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            var context = services.GetRequiredService<VoteDbContext>();

            var assemblyMigrations = context.Database.GetMigrations();
            var appliedMigrations = context.Database.GetAppliedMigrations();
            var createDatabase = !appliedMigrations.Any();
            var pendingMigrations = context.Database.GetPendingMigrations();

            var unknownMigrations = appliedMigrations.Except(assemblyMigrations);
            if (unknownMigrations.Any())
            {
                SharedHelpers.Misc.Debug.Break();

                throw new VoteException($"Application version too old. " +
                    $"Unknown applied migration(s):{string.Join(",", unknownMigrations)}. " +
                    $"Last assembly migration:{assemblyMigrations.LastOrDefault() ?? "not found"}.");
            }
            else if (pendingMigrations.Any())
            {
                context.Database.Migrate();
                if (createDatabase)
                {
                    var settings = services.GetRequiredService<Microsoft.Extensions.Options.IOptions<Models.Settings.VoteSettings>>();
                    var user = await context.User.SingleAsync(u => u.UserName == "first@user.com").ConfigureAwait(false);

                    // Initializing and assigning custom roles
                    var roleManager = services.GetRequiredService<Microsoft.AspNetCore.Identity.RoleManager<Microsoft.AspNetCore.Identity.IdentityRole>>();
                    var userManager = services.GetRequiredService<Microsoft.AspNetCore.Identity.UserManager<VoteDatabase.Models.BO.User>>();

                    await CreateRoleAndAddUser(roleManager, userManager, VoteDatabase.Constants.Roles.Administrator, user).ConfigureAwait(false);
                    await CreateRoleAndAddUser(roleManager, userManager, VoteDatabase.Constants.Roles.PiranhaAdministrator, user).ConfigureAwait(false);

                    user.PasswordHash = userManager.PasswordHasher.HashPassword(user, settings.Value.InitialAdminPassword);
                    await context.SaveChangesAsync().ConfigureAwait(false);
                }
            }
        }

        private static async Task CreateRoleAndAddUser(Microsoft.AspNetCore.Identity.RoleManager<Microsoft.AspNetCore.Identity.IdentityRole> roleManager, Microsoft.AspNetCore.Identity.UserManager<VoteDatabase.Models.BO.User> userManager, string roleName, VoteDatabase.Models.BO.User user)
        {
            await roleManager.CreateAsync(new Microsoft.AspNetCore.Identity.IdentityRole(roleName)).ConfigureAwait(false);
            await userManager.AddToRoleAsync(user, roleName).ConfigureAwait(false);
        }
    }
}
