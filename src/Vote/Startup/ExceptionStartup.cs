using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Vote.Constants;
using static System.FormattableString;
namespace Vote.Startup
{
    /// <summary>
    /// Fallback Startup used when primary Startup caused exception during initialization
    /// </summary>
    public class ExceptionStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Minimal configuration
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (Program.StartupException == null) { throw new InvalidOperationException(nameof(Program.StartupException)); }

            var startupLogFile = VoteConfig.StartupExceptionLogFilePath;

            var errorMessage = $"Configuration invalid. Please check {startupLogFile}.";

            var sb = new StringBuilder();
            sb.AppendLine(Invariant($"{Names.AppName} failed to start at UTC {DateTime.UtcNow} ({DateTime.Now})"));
            sb.AppendLine(Program.StartupException.ToString());
            sb.AppendLine("=======================");
            var errorDetails = sb.ToString();

            if (env.IsDevelopment())
            {
                errorMessage = errorDetails;
            }

            try
            {
                File.AppendAllText(startupLogFile, errorDetails);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                errorMessage = $"Configuration invalid and failed to write {startupLogFile}. Please check permissions." +
                    $"{Environment.NewLine}{Environment.NewLine}{ex}" +
                    $"{Environment.NewLine}{Environment.NewLine}{errorDetails}";
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.Map(Names.ExceptionViewUrl, async context =>
                {
                    await context.Response.WriteAsync(File.ReadAllText(startupLogFile)).ConfigureAwait(false);
                });
                endpoints.MapFallback(async context =>
                {
                    await context.Response.WriteAsync(errorMessage).ConfigureAwait(false);
                });
            });
        }
    }
}
