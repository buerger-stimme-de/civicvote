﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.User
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<VoteDatabase.Models.BO.User> mUserManager;

        public IndexModel(UserManager<VoteDatabase.Models.BO.User> userManager)
        {
            mUserManager = userManager;
            VoteUser = new List<VoteDatabase.Models.BO.User>();
        }

        public IList<VoteDatabase.Models.BO.User> VoteUser { get; private set; }

        public async Task OnGetAsync()
        {
            VoteUser = await mUserManager.Users.ToListAsync().ConfigureAwait(false);
            foreach (var user in VoteUser)
            {
                user.Roles = await mUserManager.GetRolesAsync(user).ConfigureAwait(false);
            }
        }
    }
}
