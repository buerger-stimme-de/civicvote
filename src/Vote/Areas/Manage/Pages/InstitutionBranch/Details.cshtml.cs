﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.InstitutionBranch
{
    public class DetailsModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public DetailsModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        public VoteDatabase.Models.BO.InstitutionBranch InstitutionBranch { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            InstitutionBranch = (await mContext.InstitutionBranch.FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false))!;

            return InstitutionBranch == null ? NotFound() : Page();
        }
    }
}
