﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.InstitutionBranch
{
    public class IndexModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public IndexModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        public IList<VoteDatabase.Models.BO.InstitutionBranch> InstitutionBranch { get; private set; }

        public async Task OnGetAsync()
        {
            InstitutionBranch = await mContext.InstitutionBranch.ToListAsync().ConfigureAwait(false);
        }
    }
}
