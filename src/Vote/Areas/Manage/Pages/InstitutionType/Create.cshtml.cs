﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Vote.Areas.Manage.Pages.InstitutionType
{
    public class CreateModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public CreateModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionType = null!;
        }

        public IActionResult OnGet()
        {
            ViewData["FkBranch"] = new SelectList(mContext.InstitutionBranch, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public VoteDatabase.Models.BO.InstitutionType InstitutionType { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.InstitutionType.Add(InstitutionType);
            await mContext.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
