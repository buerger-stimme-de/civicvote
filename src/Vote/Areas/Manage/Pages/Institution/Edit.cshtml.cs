using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.Institution
{
    public class EditModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public EditModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            Institution = null!;
        }

        [BindProperty]
        public VoteDatabase.Models.BO.Institution Institution { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Institution = (await mContext.Institution
                .Include(i => i.InstitutionType).FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false))!;

            if (Institution == null)
            {
                return NotFound();
            }

            ViewData["FkInstitutionType"] = new SelectList(mContext.InstitutionType, "Id", "Name");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.Attach(Institution).State = EntityState.Modified;

            try
            {
                await mContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InstitutionExists(Institution.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool InstitutionExists(uint id)
        {
            return mContext.Institution.Any(e => e.Id == id);
        }
    }
}
