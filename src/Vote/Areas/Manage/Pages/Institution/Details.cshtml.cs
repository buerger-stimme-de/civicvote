using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.Institution
{
    public class DetailsModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public DetailsModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            Institution = null!;
        }

        public VoteDatabase.Models.BO.Institution Institution { get; set; }

        public async Task<IActionResult> OnGetAsync(uint? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Institution = (await mContext.Institution
                .Include(i => i.InstitutionType).FirstOrDefaultAsync(m => m.Id == id).ConfigureAwait(false))!;

            return Institution == null ? NotFound() : Page();
        }
    }
}
