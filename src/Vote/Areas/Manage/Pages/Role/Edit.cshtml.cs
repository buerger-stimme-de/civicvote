using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vote.Areas.Manage.Pages.Role
{
    public class EditModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public EditModel(RoleManager<IdentityRole> roleManager)
        {
            Role = null!;
            mRoleManager = roleManager;
        }

        [BindProperty]
        public IdentityRole Role { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Role = await mRoleManager.FindByIdAsync(id).ConfigureAwait(false);

            return Role == null ? NotFound() : Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var role = await mRoleManager.FindByIdAsync(Role.Id).ConfigureAwait(false);
            if (role == null)
            {
                return NotFound();
            }

            await mRoleManager.SetRoleNameAsync(role, Role.Name).ConfigureAwait(false);
            await mRoleManager.UpdateAsync(role).ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
