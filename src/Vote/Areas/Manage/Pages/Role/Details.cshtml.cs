using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vote.Areas.Manage.Pages.Role
{
    public class DetailsModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public DetailsModel(RoleManager<IdentityRole> roleManager)
        {
            Role = null!;
            mRoleManager = roleManager;
        }

        public IdentityRole Role { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Role = await mRoleManager.FindByIdAsync(id).ConfigureAwait(false);

            return Role == null ? NotFound() : Page();
        }
    }
}
