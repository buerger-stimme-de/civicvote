﻿namespace Vote.Constants
{
    public static class Version
    {
        /// <summary>
        /// Version of web application.
        /// </summary>
        public const string VersionString = "0.0.2";
    }
}
