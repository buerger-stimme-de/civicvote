﻿namespace Vote.Constants
{
    public static class PermissionPolicy
    {
        public const string Manage = "ManageArea";
        public const string Cms = "Cms";

        public static string[] All()
        {
            return new[]
            {
                Manage,
                Cms,
            };
        }
    }
}
