﻿namespace Vote.Constants
{
    public static class VoteConfig
    {
        /// <summary>
        /// Relative path to folder where configuration data and logs are stored.
        /// </summary>
        public const string ConfigDirectory = "config";

        /// <summary>
        /// Fallback log file used when exception occurred during initialization.
        /// </summary>
        public const string StartupExceptionLogFile = "startupException.log";
        public static string StartupExceptionLogFilePath => System.IO.Path.Combine(ConfigDirectory, StartupExceptionLogFile);

        /// <summary>
        /// Required configuration file with default settings for CivicVote. This file is committed to Git and should only be
        /// changed by developers when new settings are added for which sensible default values can be provided.
        /// </summary>
        public const string AppSettingsDefaultFile = "appsettings.default.json";
        public static string AppSettingsDefaultFilePath => System.IO.Path.Combine(ConfigDirectory, AppSettingsDefaultFile);

        /// <summary>
        /// Optional configuration file with user-defined settings for CivicVote.
        /// </summary>
        public const string AppSettingsFile = "appsettings.json";
        public static string AppSettingsFilePath => System.IO.Path.Combine(ConfigDirectory, AppSettingsFile);

        /// <summary>
        /// Path to configuration file for Tiny MCE Editor.
        /// </summary>
        public const string TinyMceEditorConfigFile = "editorconfig.json";
        public static string TinyMceEditorConfigFilePath => System.IO.Path.Combine(ConfigDirectory, TinyMceEditorConfigFile);

        /// <summary>
        /// Set to false, to disable user registration page.
        /// </summary>
        public static bool EnableRegisterPage { get; }
    }
}
