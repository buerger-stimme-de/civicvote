﻿using System.ComponentModel.DataAnnotations;

namespace Vote.Models.Settings
{
    public class VoteSettings
    {
        public const string ErrorMessageRequiredValue = "Please define \"{0}\" in '" + Constants.VoteConfig.ConfigDirectory + "/" + Constants.VoteConfig.AppSettingsFile + "' or via system variable";

        /// <summary>
        /// Connection details for accessing database for CivicVote
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public MySqlSettings DatabaseConnectionVote { get; set; } = null!;

        /// <summary>
        /// Connection details for accessing database for CivicVote's integrated Piranha CMS
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public MySqlSettings DatabaseConnectionCms { get; set; } = null!;

        /// <summary>
        /// Initial admin password. It is set for user "first@user.com" during initializing the database.
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public string InitialAdminPassword { get; set; } = null!;

        /// <summary>
        /// If true, each web request is logged to database table WebRequest.
        /// </summary>
        public bool EnableRequestLogging { get; set; }

        /// <summary>
        /// Path to directory where Piranha CMS stores its upload content.
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public string UploadsCmsPath { get; set; } = null!;

        /// <summary>
        /// Path to directory where upload content of CivicVote is stored.
        /// </summary>
        [Required(ErrorMessage = ErrorMessageRequiredValue)]
        public string UploadsVotePath { get; set; } = null!;

        /// <summary>
        /// Version of commit tag of GitLab of current version of CivicVote.
        ///
        /// See https://gitlab.com/buerger-stimme-de/civicvote/-/tags
        /// </summary>
        /// <remarks>Set by GitLab's CI/CD pipeline via 'docker-compose.yml'</remarks>
        public string? TagVersion { get; set; }
    }
}
