﻿using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Vote.Models.Cms.Blocks
{
    [BlockType(Name = "H1 Header", Category = "Html forms", Icon = "fas fa-heading", IsGeneric = true)]
    public class Header1Block : Block
    {
        /// <summary>
        /// Name of upload element
        /// </summary>
        [Field(Title = "H1 Header text")]
        public StringField HeaderText { get; set; } = null!;
    }
}
