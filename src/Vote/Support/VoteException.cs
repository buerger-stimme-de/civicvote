﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vote.Support
{
    public class VoteException : Exception
    {
        public VoteException()
            : base()
        {
        }

        public VoteException(string message)
            : base(message)
        {
        }

        public VoteException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
