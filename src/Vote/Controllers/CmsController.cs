﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Piranha.Manager;
using Vote.Constants;
using Vote.Models;
using Vote.Support;
using VoteDatabase.DataAccess;

namespace Vote.Controllers
{
    [Authorize(Policy = PermissionPolicy.Cms)]
    public class CmsController : Controller
    {
        public static string Name { get; } = nameof(CmsController)[..^10];
        public static string ActionIndex { get; } = nameof(Index);

        private readonly IAntiforgery mAntiForgery;
        private readonly ManagerOptions mOptions;

        public CmsController(IAntiforgery antiforgery, IOptions<ManagerOptions> options)
        {
            if (options == null) { throw new ArgumentNullException(nameof(options)); }
            mAntiForgery = antiforgery;
            mOptions = options.Value;
        }

        public IActionResult Index()
        {
            // Actually we should be able to forward like:
            // return LocalRedirect("~/manager/login/auth");
            // But this does not work due to https://github.com/PiranhaCMS/piranha.core/issues/1829
            // Workaround: Set anti forgery cookie directly.

            var tokens = mAntiForgery.GetAndStoreTokens(HttpContext);
            var requestToken = tokens.RequestToken ?? throw new InvalidOperationException("AntiForgery RequestToken null");
            Response.Cookies.Append(mOptions.XsrfCookieName, requestToken, new CookieOptions
            {
                HttpOnly = false,
                IsEssential = true,
            });

            return LocalRedirect("~/manager");
        }
    }
}
