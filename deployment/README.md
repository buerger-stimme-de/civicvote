# Deployment
Contains all scripts for Ansible to setup a server running CivicVote.

# Server Preparation
Ansible needs a SSH key to connect to the server. Thus, the following setup has to be
done on the server before:

1. Install the newest Debian as operating system
2. Create a user for deployment (used by Ansible)
    ```bash
    sudo useradd deploy
    sudo usermod -aG sudo deploy

    mkdir /home/deploy/.ssh
    ssh-keygen -t ecdsa -b 384 -f /home/deploy/.ssh/id_ecdsa -C "Gitlab CI key used for deployment" -N ""

    chown -R deploy:deploy /home/deploy
    ```
3. Use `sudo visudo` to add the `deploy` user. Append the following line: `deploy ALL=(ALL) NOPASSWD:ALL`
3. Remove `/home/deploy/.ssh/id_ecdsa` file and add the content to the Gitlab top level group as protected token named `SSH_KEY_DEPLOY`.
4. Add `/home/deploy/.ssh/id_ecdsa.pub` to `/home/deploy/.ssh/authorized_keys` and remove the `id_ecdsa.pub` file afterwards.
