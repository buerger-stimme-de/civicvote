#!/usr/bin/bash

# start this script the very first time only
# - it creates dummy certificates so the Nginx starts
# - it initializes the Certbot process by retrieving the first certificate
# - it restarts Nginx

echo "### Creating certificate path"
mkdir -p "certbot/live/{{primary_server_name}}"
mkdir -p "certbot/archive/{{primary_server_name}}"
mkdir -p "certbot/renewal/{{primary_server_name}}"
mkdir -p "/etc/letsencrypt/live/{{primary_server_name}}"

echo "### Creating dh-params.pem"
docker-compose run --rm --entrypoint "openssl dhparam -out /etc/letsencrypt/dh-params.pem 4096" certbot

echo "### Creating dummy certificate for localhost"
docker-compose run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:4096 -days 1\
    -keyout '/etc/letsencrypt/live/{{primary_server_name}}/privkey.pem' \
    -out '/etc/letsencrypt/live/{{primary_server_name}}/fullchain.pem' \
    -subj '/CN=localhost'" certbot

echo "### Starting nginx ..."
docker-compose up --force-recreate -d nginx

echo "### Deleting dummy certificate for localhost"
rm -Rf certbot/live/{{primary_server_name}}
rm -Rf certbot/archive/{{primary_server_name}}
rm -Rf certbot/renewal/{{primary_server_name}}.conf

echo "### Requesting Let's Encrypt certificates"
domain_args=""
for domain in {{server_names}}; do
  domain_args="$domain_args -d $domain"
done

email_arg="--email {{letsencrypt_email_address}}"
staging_arg=""

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --rsa-key-size 4096 \
    --agree-tos \
    -n \
    --force-renewal" certbot

echo "### Reloading nginx"
docker-compose exec nginx nginx -s reload
